export interface Customer{
        fullName: string,
        firstName: string,
        lastName: string,
        telephone: string,
        mobile: string,
        streetAddress1: string,
        streetAddress2: string,
        city: string,
        state: string,
        postalCode: string,
        country: string,
        codeNumber: string,
        invoicingMethod: string,
        currency: string,
        email: string,
        category: string,
        notes: string
}