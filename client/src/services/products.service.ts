import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment'
 
@Injectable({
    providedIn: 'root'
})
export class ProductService {
    baseUrl: string = `${environment.apiBaseUrl}`;
    
    constructor(private http: HttpClient){

    }

    public getProducts(): Observable<any>{
        return this.http.get(`${this.baseUrl}/products`);
    }
    public getProductsFromID(id): Observable<any>{
        return this.http.get(`${this.baseUrl}/products/${id}`);
    }
}