import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment'
 
@Injectable({
    providedIn: 'root'
})
export class UserService {
    baseUrl: string = `${environment.apiBaseUrl}`;
    
    constructor(private http: HttpClient){

    }

    public getUsers(): Observable<any>{
        return this.http.get(`${this.baseUrl}/users`);
    }
    public createUser(userDetail): Observable<any>{
        return this.http.post(`${this.baseUrl}/users/create`,userDetail);
    }
}