import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AppHeaderComponent } from './components/app-header/app-header.component';
import { MainBodyComponent } from './components/main-body/main-body.component';
import { BetweenHeadBodyComponent } from './components/between-head-body/between-head-body.component';
import { FinalComponent } from './components/final/final.component';
import { ProductsBodyComponent } from './components/products-body/products-body.component';
import { UsersComponentComponent } from './components/users-component/users-component.component';
import { FormsModule } from '@angular/forms'; 
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    MainBodyComponent,
    BetweenHeadBodyComponent,
    FinalComponent,
    ProductsBodyComponent,
    UsersComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
