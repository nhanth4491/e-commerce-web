import { Component, OnInit } from '@angular/core';

declare var $;

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  
  ngAfterViewInit(){
    $('.logo').attr("src","\\assets\\images\\LogoM.png");
    
  }

}
