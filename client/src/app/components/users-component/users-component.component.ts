import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/services/users.service';
import { Customer } from 'src/models/customer';

@Component({
  selector: 'app-users-component',
  templateUrl: './users-component.component.html',
  styleUrls: ['./users-component.component.scss']
})
export class UsersComponentComponent implements OnInit {
  customer : Customer = {fullName: '',
                        firstName: '',
                        lastName: '',
                        telephone: '',
                        mobile: '',
                        streetAddress1: '',
                        streetAddress2: '',
                        city: '',
                        state: '',
                        postalCode: '',
                        country: '',
                        codeNumber: '',
                        invoicingMethod: '',
                        currency: '',
                        email: '',
                        category: '',
                        notes: ''
                      };
  user : any;
  constructor(private userService:UserService) { }

  ngOnInit(): void {
  }

  saveCustomer(e){
    this.userService.createUser(this.customer).subscribe(rs => {
      console.log(rs);
    })
  }
}
