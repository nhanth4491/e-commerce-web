import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/services/products.service';
import { ActivatedRoute } from '@angular/router';

declare var $;

@Component({
  selector: 'app-products-body',
  templateUrl: './products-body.component.html',
  styleUrls: ['./products-body.component.scss']
})
export class ProductsBodyComponent implements OnInit {

  product: any = {};
  images: Array<any> = [];
  constructor(private activatedRoute: ActivatedRoute, private productsService: ProductService) {
  }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      const id: Number = Number(params.get('id'));
      this.productsService.getProductsFromID(id).subscribe(product => {

        let firstImageSrc: string = product.src;
        let scrWithoutExtension = firstImageSrc.substring(0, firstImageSrc.lastIndexOf('.') - 1);
        for (let i = 0; i < product.soluong; i++) {
          this.images.push(scrWithoutExtension + i + '.jpg');
        }
        this.product = product;
      });
    });
  }

  ngAfterViewInit() {

  }
  clickItemPictureBig(e) {
    if (!$(e.target).is(".img-big-show")) {
      $(".item-picture-big").hide();
    }
    if ($(e.target).is(".button-custom-next") || $(e.target).is("#btn-next")) {
      $(".item-picture-big").show();
      let imgSource = $(".img-big-show").attr("src");
      let check = imgSource.slice(imgSource.length - 5, imgSource.length - 4);
      if (check == this.product.soluong - 1)
        check = 0;
      else
        check++;
      imgSource = imgSource.substring(0, imgSource.length - 5) + check + ".jpg";
      $(".img-big-show").attr("src", imgSource);
    }
    if ($(e.target).is(".button-custom-back") || $(e.target).is("#btn-back")) {
      $(".item-picture-big").show();
      let imgSource = $(".img-big-show").attr("src");
      let check = imgSource.slice(imgSource.length - 5, imgSource.length - 4);
      if (check == 0)
        check = this.product.soluong - 1;
      else
        check--;
      imgSource = imgSource.substring(0, imgSource.length - 5) + check + ".jpg";
      $(".img-big-show").attr("src", imgSource);
    }
    if($(e.target).is(".image-mini img")|| $(e.target).is("ul")||  $(e.target).is("ul .image-mini")){
      $(".item-picture-big").show();
    }
  };
  showImageBig(e) {
    let imgSource = $(e.target).attr("src");
    $(".item-picture-big .img-big-show").attr("src", imgSource);
    $(".item-picture-big").show();
  };
  MiniImgCLick(e){
    let src = $(e.target).attr("src");
    $(".img-big-show").attr("src", src);
  };
}


