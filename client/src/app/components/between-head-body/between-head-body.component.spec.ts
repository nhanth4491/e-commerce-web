import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetweenHeadBodyComponent } from './between-head-body.component';

describe('BetweenHeadBodyComponent', () => {
  let component: BetweenHeadBodyComponent;
  let fixture: ComponentFixture<BetweenHeadBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetweenHeadBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetweenHeadBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
