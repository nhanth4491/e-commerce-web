import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/services/products.service';

declare var $;

@Component({
  selector: 'app-main-body',
  templateUrl: './main-body.component.html',
  styleUrls: ['./main-body.component.scss']
})
export class MainBodyComponent implements OnInit {
  intervalHandle;
  products: Array<any>;
  //images: Array<any>;

  constructor(private productsService: ProductService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.productsService.getProducts().subscribe(result => {
      for(let p of result){
        let src = [];
        let firstImageSrc: string = p.src;
        let scrWithoutExtension = firstImageSrc.substring(0,firstImageSrc.lastIndexOf('.')-1);
        for(let i = 0; i < p.soluong; i++){
          src.push(scrWithoutExtension + i + '.jpg');
        }
        p.products = src;
      }
      this.products = result;
    });



    $('.item-search-left').click(function () {
      console.log("click");
      var txtsearch = $('.search-box-left');
      txtsearch.toggleClass('expand');
    });
    $('.atsa-item').hover(function () {
      $(this).toggleClass("background-color-gray");
    });

    $('.checkbox-custom').click(function () {
      $(this).toggleClass('checked');
      $(this).toggleClass('checkbox-custom-backroundcolor');
    });

    $('.menu-item').hover(function () {
      var bodyShadow = $('.body-shadow');
      bodyShadow.toggleClass('body-shadow-show');
    });

    $('.atsa-item').click(function () {
      $('.header-expand').toggleClass("display-show-hidden");
    });

    $('.select-box').hover(function () {
      $('.select-box-option').toggleClass("visible-select-box")
    });
    $('.name-sortBy').hover(function () {
      $(this).css({ "cursor": "default" })
      $('.select-box-option').toggleClass("visible-select-box")
    });
    $('.select-box-option').hover(function () {
      $(this).toggleClass("visible-select-box");
    });
    $('.li-option-select').hover(function () {
      $(this).css({ "cursor": "default" })
      $(this).toggleClass("background-color-gray");
    });
    // $('.li-option-select').click(function () {
    //   var str = $(this).text();
    //   $('.text-select').text(str);
    // })
  }

  hoverOptionSelect(e){
    console.log(e);
    $(e.target).css({ "cursor": "default" });
    $(e.target).toggleClass("background-color-gray");
  }
  clickOptionSelect(e){
    var str =$(e.target).text();
    $('.text-select').text(str);   
  }
  mouseEnter(e){
    var group = $(e.target).find(".group-img");
      var count = $(group).children().length;
      var i = 0;
      this.intervalHandle = setInterval(() => {
        if (i < count)
          i++;
        else
          i = 1;
        this.setPos(group, i, count);
      }, 1300);
  }
  mouseLeave(e){
    clearInterval(this.intervalHandle);
    var group = $(e.target).find(".group-img");
    $(group).css({ "left": 0 });
  }
  setPos(group, i, count) {
    var pos = $(group).position().left;
    if (i < count) {
      pos -= 200;
      $(group).css({ "left": pos });
    }
    else {
      $(group).css({ "left": 0 });
    }
  }
}
