import { Component, AfterViewInit } from '@angular/core';
import { UserService } from 'src/services/users.service';
import { from } from 'rxjs';
import { ProductService } from 'src/services/products.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = 'e-commerce';
  users: Array<any>;
  products: Array<any>;

  constructor(private userService: UserService,private productsService: ProductService){

  };

  ngAfterViewInit(){
    this.userService.getUsers().subscribe(result => {
      // console.log(result);
      this.users = result;
    });

    this.productsService.getProducts().subscribe(result =>{
      this.products = result;
    })
  }
}
