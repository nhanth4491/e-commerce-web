import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsBodyComponent } from './components/products-body/products-body.component';
import { MainBodyComponent } from './components/main-body/main-body.component';


const routes: Routes = [
  {path: '', component:MainBodyComponent},
  {path: 'products/:id',component: ProductsBodyComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
