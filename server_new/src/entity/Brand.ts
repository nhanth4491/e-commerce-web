import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Brand {

    @PrimaryGeneratedColumn()
    ID: number;

    @Column()
    Name: string;

    @Column()
    Phonenumber: string;

    @Column()
    Address: string;

}
