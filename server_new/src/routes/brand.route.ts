import * as express from "express";
import * as brandController from "../controller/BrandController";

const router = express.Router();


router.get('/all', brandController.all);
router.post('/delete/:id', brandController.deleteUser);


export default router;