import * as express from "express";
import * as roleController from '../controller/RoleController';
const router = express.Router();

router.get('/all', roleController.all);
router.get('/delete/:id', roleController.remove);
router.post('/create', roleController.insert);
router.post('/update', roleController.update);

export default router;