import * as express from 'express';


import brandRoute from './brand.route';

import productRoute from './product.route';
import roleRoute from './role.route';



const router = express.Router();


router.use('/api/brand', brandRoute);

router.use('/api/product', productRoute);
router.use('/api/role', roleRoute);



export default router;