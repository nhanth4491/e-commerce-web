import * as express from "express";
import * as productController from '../controller/ProductController';
const router = express.Router();


router.get('/all', productController.all);
router.get('/delete/:id', productController.remove);
router.post('/insert', productController.insert);
router.post('/update', productController.update);

export default router;