import { Request, Response } from "express";
import { ProductService } from '../services/product.service';
import { Product } from "../entity/Product";

export async function all(req: Request, res: Response) {    
    var rs = await ProductService.Ins.getAll();
    res.json(rs);
}
export async function byId(req: Request, res: Response){
    var rs = await ProductService.Ins.getById(req.query.id);
    res.json(rs);
}
export async function insert(req: Request, res: Response) {
    console.log(req.body);
    let newProduct = new Product();
    newProduct.name = req.body.name;
    newProduct.category = req.body.category;
    var rs = await ProductService.Ins.insert(newProduct);
    res.json(rs);
}
export async function update(req: Request, res: Response) {
    let product = new Product();
    product.name = req.body.name;
    product.category = req.body.category;

    var rs = await ProductService.Ins.update(product);
    res.json(rs);
}
export async function remove(req: Request, res: Response) {    
    var rs = await ProductService.Ins.delete(req.query.id);
    res.json(rs);
}

