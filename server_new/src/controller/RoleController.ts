import { Request, Response } from "express";
import { RoleService } from '../services/role.service';
import { Role } from "../entity/Role";

export async function all(req: Request, res: Response) {    
    var rs = await RoleService.Ins.getAll();
    res.json(rs);
}
export async function byId(req: Request, res: Response){
    var rs = await RoleService.Ins.getById(req.query.id);
    res.json(rs);
}
export async function insert(req: Request, res: Response) {
    console.log(req.body);
    let newRole = new Role();
    newRole.ID = req.body.ID;
    newRole.Role = req.body.Role;
    var rs = await RoleService.Ins.insert(newRole);
    res.json(rs);
}
export async function update(req: Request, res: Response) {
    let role = new Role();
    role.ID = req.body.ID;
    role.Role = req.body.Role;

    var rs = await RoleService.Ins.update(role);
    res.json(rs);
}
export async function remove(req: Request, res: Response) {    
    var rs = await RoleService.Ins.delete(req.query.id);
    res.json(rs);
}

