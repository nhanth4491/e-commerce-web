import {getRepository, getConnectionManager, Repository} from "typeorm";
import { Brand } from "../entity/Brand";

export class BrandService {
    brandRespository: Repository<Brand>;
    public static _ins: BrandService;
    public static get Ins(): BrandService {
        if(!BrandService._ins){
            BrandService._ins = new BrandService();
        }
        return BrandService._ins;
    }
    constructor(){
        this.brandRespository = getRepository(Brand);
    }

    public getAll(){
        return this.brandRespository.query("SELECT * FROM Brand");
    }
    public deleteProduct(prdId: string){
        return this.brandRespository.query(`DELETE FROM Brand WHERE id = ${prdId}`);
    }

    public insert(userObj: Brand){
        return this.brandRespository.save(userObj);
    }
}