import {getRepository, Repository} from "typeorm";
import { Product } from "../entity/Product";
export class ProductService {
    public static _ins: ProductService;
    public static get Ins(): ProductService {
        if(!ProductService._ins){
            ProductService._ins = new ProductService();
        }
        return ProductService._ins;
    }

    repository: Repository<Product>;
    
    constructor(){
        this.repository = getRepository(Product);
    }

    public getAll(){
        //return this.repository.query("SELECT * FROM PRODUCT");
        return this.repository.find();
    }
    public getById(id: number){
        //return this.repository.query(`SELECT * FROM PRODUCT WHERE id = ${id}`);
        return this.repository.findOne(id);
    }
    public insert(product: Product){
        return this.repository.save(product);
    }
    public async update(product: Product){
        let _product = await this.repository.findOne(product.id);
        return this.repository.save(_product);
    }
    public async delete(id: number){
        //return this.repository.query(`DELETE FROM PRODUCT WHERE id = ${id}`);
        let prd = await this.repository.findOne(id);
        return this.repository.delete(prd);
    }

    
}