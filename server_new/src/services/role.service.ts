import {getRepository, Repository} from "typeorm";
import { Role } from "../entity/Role";
export class RoleService {
    public static _ins: RoleService;
    public static get Ins(): RoleService {
        if(!RoleService._ins){
            RoleService._ins = new RoleService();
        }
        return RoleService._ins;
    }

    repository: Repository<Role>;
    
    constructor(){
        this.repository = getRepository(Role);
    }

    public getAll(){
        //return this.repository.query("SELECT * FROM PRODUCT");
        return this.repository.find();
    }
    public getById(id: number){
        //return this.repository.query(`SELECT * FROM PRODUCT WHERE id = ${id}`);
        return this.repository.findOne(id);
    }
    public insert(role: Role){
        return this.repository.save(role);
    }
    public async update(role: Role){
        let _role = await this.repository.findOne(role.ID);
        return this.repository.save(_role);
    }
    public async delete(ID: number){
        //return this.repository.query(`DELETE FROM PRODUCT WHERE id = ${id}`);
        let role = await this.repository.findOne(ID);
        return this.repository.delete(role);
    }

    
}