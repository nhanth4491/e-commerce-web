import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment'
 
@Injectable({
    providedIn: 'root'
})
export class UserService {
    baseUrl: string = `${environment.apiBaseUrl}`;
    
    constructor(private http: HttpClient){

    }

    public getUsers(): Observable<any>{
        return this.http.get(`${this.baseUrl}/users`);
    }
    public getUserFromID(id): Observable<any>{
        return this.http.get(`${this.baseUrl}/users/getFromID/${id}`);
    }
    public deleteUser(id): Observable<any>{
        return this.http.get(`${this.baseUrl}/users/delete/${id}`);
    }
    public createUser(userDetail): Observable<any>{
        return this.http.post(`${this.baseUrl}/users/create`,userDetail);
    }
    public updateUser(userDetail): Observable<any>{
        return this.http.post(`${this.baseUrl}/users/updateUser`,userDetail);
    }
}