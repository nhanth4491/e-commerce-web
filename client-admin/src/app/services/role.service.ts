import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment'
 
@Injectable({
    providedIn: 'root'
})
export class RoleService {
    baseUrl: string = `${environment.apiBaseUrl}`;
    
    constructor(private http: HttpClient){
        
    }
    public createRole(role): Observable<any>{
        return this.http.post(`${this.baseUrl}/role/create`,role);
    }
    
}