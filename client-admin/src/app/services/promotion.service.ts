import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment'
 
@Injectable({
    providedIn: 'root'
})
export class PromotionService {
    baseUrl: string = `${environment.apiBaseUrl}`;
    
    constructor(private http: HttpClient){
        
    }
    public createPromotion(promotion): Observable<any>{
        return this.http.post(`${this.baseUrl}/promotions/create`,promotion);
    }
    
}