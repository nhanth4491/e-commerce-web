import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment'
 
@Injectable({
    providedIn: 'root'
})
export class BrandService {
    baseUrl: string = `${environment.apiBaseUrl}`;
    
    constructor(private http: HttpClient){

    }

    public getBrands(): Observable<any>{
        return this.http.get(`${this.baseUrl}/brands`);
    }
    public getByID(ID): Observable<any>{
        return this.http.get(`${this.baseUrl}/brands/getByID/${ID}`);
    }
    public create(brandDetail): Observable<any>{
        return this.http.post(`${this.baseUrl}/brands/create`,brandDetail);
    }
    public update(brandDetail): Observable<any>{
        return this.http.post(`${this.baseUrl}/brands/update`,brandDetail);
    }
    public delete(id): Observable<any>{
        return this.http.get(`${this.baseUrl}/brands/deleteByID/${id}`);
    }
}