import { AbstractControl } from '@angular/forms';
export class ConfirmPasswordValidator {
  static MatchPassword(control: AbstractControl) {
    let password = control.get('Password').value;
    let confirmPassword = control.get('ConfirmPassword').value;

    if(password != '' && confirmPassword != ''){
      if (password != confirmPassword) {
        control.get('ConfirmPassword').setErrors({ MatKhauKhongKhop: true });
      }
      else {
        //control.get('ConfirmPassword').setErrors({ MatKhauKhongKhop: false });
      }
    } 
  }
}