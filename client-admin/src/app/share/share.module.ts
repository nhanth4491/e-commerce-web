import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
import { MyTableComponent } from '../components/my-table/my-table.component';
import { RouterModule } from '@angular/router';
import { routes } from '../app-routing.module';



@NgModule({
    imports:      [ CommonModule, BrowserModule,RouterModule.forRoot(routes) ],
    declarations: [ MyTableComponent ],
    exports:      [ MyTableComponent ]
   })
   export class SharedModule { }