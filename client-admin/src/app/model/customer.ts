export interface Customer{
    id: number,
    userName: string,
    passWord: string,
    firstName: string,
    lastName: string,
    fullName: string,
    Address: string,
    mobile: string,
    email: string,
    userRole: string,
}