import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Customer } from '../../model/customer';
import { UserService } from '../../services/customer.service';

@Component({
  selector: 'my-table',
  templateUrl: './my-table.component.html',
  styleUrls: ['./my-table.component.scss']
})
export class MyTableComponent implements OnInit {
  // columns = [
  //   { name: 'STT' },
  //   { name: 'First Name' },
  //   { name: 'Last Name' },
  //   { name: 'Full Name' },
  //   { name: 'Email' },
  //   { name: 'Mobile' }
  // ]

@Input('columns') columns: Array<any>;
@Input('rows') rows: Array<Customer>;
@Input('index') index:boolean;
@Input('idKey') idKey:string;
@Input('router') router:string;




@Output('onDelete') onDeleteEvt = new EventEmitter<any>();
// @Output('routerLink') routerLink = new EventEmitter<any>();
  
  // valuesUsers : Customer =  {
  //   id:null,
  //   fullName: '',
  //   firstName: '',
  //   lastName: '',
  //   telephone: '',
  //   mobile: '',
  //   streetAddress1: '',
  //   streetAddress2: '',
  //   city: '',
  //   state: '',
  //   postalCode: '',
  //   country: '',
  //   codeNumber: '',
  //   invoicingMethod: '',
  //   currency: '',
  //   email: '',
  //   category: '',
  //   note: ''
  // };
  // rows:Array<Customer> = [this.valuesUsers];
  constructor(private userService:UserService) { }

  ngOnInit(): void {
    console.log(this.router)
  }

  onDelete(row: Customer): void {
    console.log(row);

    this.onDeleteEvt.emit(row);
  }

}
