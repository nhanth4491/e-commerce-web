import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'create-customerGroup',
  templateUrl: './create-customer-group.component.html',
  styleUrls: ['./create-customer-group.component.scss']
})
export class CreateCustomerGroupComponent implements OnInit {
  rows = [];
  columns = [
    {name:'ID',dataKey:'id'},
    {name:'First Name',dataKey:'firstname'},
    {name:'Last Name',dataKey:'lastname'},
    {name:'Full Name',dataKey:'fullname'},
    {name:'Telephone',dataKey:'telephone'},
    {name:'Email', dataKey:'email'},  
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
