import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { UserListComponent } from './user-list/user-list.component';
import { CreateProductComponent } from './create-product/create-product.component';
import { CreateBrandComponent } from './create-brand/create-brand.component';
import { CreateCategoryComponent } from './create-category/create-category.component';
import { CreateCustomerGroupComponent } from './create-customer-group/create-customer-group.component';
import { CreateNewsComponent } from './create-news/create-news.component';
import { CreateImportStockOrderComponent } from './create-import-stock-order/create-import-stock-order.component';
import { CreatePromotionComponent } from './create-promotion/create-promotion.component';
import { CreateRoleComponent } from './create-role/create-role.component';
import { ListBrandsComponent } from './list-brands/list-brands.component';
import { ListRoleComponent } from './list-role/list-role.component';

import { TestEditorComponent } from './test-editor/test-editor.component';


const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    { path: 'test', component: TestEditorComponent },
    {
      path: 'dashboard',
      component: ECommerceComponent,
    },
    {
      path: 'iot-dashboard',
      component: DashboardComponent,
    },
    {
      path: 'create-user',
      component: CreateUserComponent,
    },
    {
      path: 'users',
      component: UserListComponent,
    },
    {
      path: 'create-product',
      component: CreateProductComponent,
    },
    {
      path: 'create-promotion',
      component: CreatePromotionComponent,
    },
    {
      path: 'create-role',
      component: CreateRoleComponent,
    },
    {
      path: 'list-role',
      component: ListRoleComponent,
    },
    {
      path: 'create-brand',
      component: CreateBrandComponent,
    },
    {
      path: 'create-brand/:id',
      component: CreateBrandComponent,
    },
    {
      path: 'list-brands',
      component: ListBrandsComponent,
    },
    {
      path: 'create-category',
      component: CreateCategoryComponent,
    },
    {
      path: 'create-user/:id',
      component: CreateUserComponent,
    },
  
    {
      path: 'create-customer-group',
      component: CreateCustomerGroupComponent,
    },
    {
      path: 'create-import-stock-order',
      component: CreateImportStockOrderComponent,
    },
    {
      path: 'create-news',
      component: CreateNewsComponent,
    },
    {
      path: 'layout',
      loadChildren: () => import('./layout/layout.module')
        .then(m => m.LayoutModule),
    },
    {
      path: 'forms',
      loadChildren: () => import('./forms/forms.module')
        .then(m => m.FormsModule),
    },
    {
      path: 'ui-features',
      loadChildren: () => import('./ui-features/ui-features.module')
        .then(m => m.UiFeaturesModule),
    },
    {
      path: 'modal-overlays',
      loadChildren: () => import('./modal-overlays/modal-overlays.module')
        .then(m => m.ModalOverlaysModule),
    },
    {
      path: 'extra-components',
      loadChildren: () => import('./extra-components/extra-components.module')
        .then(m => m.ExtraComponentsModule),
    },
    {
      path: 'maps',
      loadChildren: () => import('./maps/maps.module')
        .then(m => m.MapsModule),
    },
    {
      path: 'charts',
      loadChildren: () => import('./charts/charts.module')
        .then(m => m.ChartsModule),
    },
    {
      path: 'editors',
      loadChildren: () => import('./editors/editors.module')
        .then(m => m.EditorsModule),
    },
    {
      path: 'tables',
      loadChildren: () => import('./tables/tables.module')
        .then(m => m.TablesModule),
    },
    {
      path: 'miscellaneous',
      loadChildren: () => import('./miscellaneous/miscellaneous.module')
        .then(m => m.MiscellaneousModule),
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
