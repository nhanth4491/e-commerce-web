import { Component, OnInit } from '@angular/core';
import { Customer } from '../../model/customer';
import { UserService } from '../../services/customer.service';
import { ActivatedRoute } from '@angular/router';
import { NbToastrService, NbComponentStatus, NbGlobalPhysicalPosition } from '@nebular/theme';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfirmPasswordValidator } from '../../utils/ConfirmPasswordValidator';
import * as moment from 'moment';

@Component({
  selector: 'ngx-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  checkedID;
  constructor(private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private toastrService: NbToastrService,
    private formBuilder: FormBuilder) { }



  ngOnInit(): void {

    this.registerForm = this.formBuilder.group({
      Username: ['', Validators.required],
      Password: ['', Validators.required],
      ConfirmPassword: ['', Validators.required],
      Fullname: ['', Validators.required],
      Address: ['', Validators.required],
      Phonenumber: ['', Validators.required],
      Sex: ['',],
      Email: ['', Validators.required],
      Dateofbirth: ['', Validators.required],
      role_ID: ['1'],
      Customergroup_ID: ['1']
    }, { validator: ConfirmPasswordValidator.MatchPassword });

    this.activatedRoute.paramMap.subscribe(params => {
      this.checkedID = params.get('id');
    });
    // console.log(this.checkedID);

  }

  get f() { return this.registerForm.controls; }

  saveCustomer() {
    this.registerForm.value.Dateofbirth = moment(this.registerForm.value.Dateofbirth).format('YYYY-MM-DD');
    // console.log(this.registerForm.value.Dateofbirth);
  }

  save() {
    this.userService.createUser(this.registerForm.value).subscribe(rs => {
      this.showToast('success', 'Title', 'Tạo tài khoản thành công..!');
    })
  }
  update() {
    this.userService.updateUser(this.registerForm.value).subscribe(rs => {
      this.showToast('success', 'Title', 'Cập nhật tài khoản thành công..!');
    })
  }
  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? `. ${title}` : '';
    this.toastrService.show(
      body,
      `Toast`,
      config);
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    console.log(this.registerForm.value);
    this.saveCustomer();
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}


