import { NgModule } from '@angular/core';
import { NbMenuModule, NbCardModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { CreateUserModule } from './create-user/create-user.module';
import { CreateProductModule } from './create-product/create-product.module';

import { CreateCategoryComponent } from './create-category/create-category.component';
import { CreateNewsComponent } from './create-news/create-news.component';
import { CreateNewsModule } from './create-news/create-news.module';
import { CreateImportStockOrderModule } from './create-import-stock-order/create-import-stock-order.module';
import { CreatePromotionComponent } from './create-promotion/create-promotion.component';
import { CreateRoleComponent } from './create-role/create-role.component';

import { CKEditorComponent } from './editors/ckeditor/ckeditor.component';
import { ListRoleComponent } from './list-role/list-role.component';
import { TestEditorComponent } from './test-editor/test-editor.component';
import { TestEditorModule } from './test-editor/test-editor.module';


@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    CreateUserModule,
    CreateProductModule,
    CreateNewsModule,
    CreateImportStockOrderModule,
    NbCardModule,
    TestEditorModule
  ],
  declarations: [
    PagesComponent
  ],
})
export class PagesModule {
}
