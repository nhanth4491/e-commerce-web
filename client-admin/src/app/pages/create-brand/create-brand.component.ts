import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbToastrService, NbComponentStatus, NbGlobalPhysicalPosition } from '@nebular/theme';
import { BrandService } from '../../services/brand.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-create-brand',
  templateUrl: './create-brand.component.html',
  styleUrls: ['./create-brand.component.scss']
})
export class CreateBrandComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  paramsID : string;
  check: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private brandService: BrandService,
    private toastrService: NbToastrService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    this.registerForm = this.formBuilder.group({
      ID: [''],
      Name: ['', Validators.required],
      Phonenumber: ['', Validators.required],
      Address: ['', Validators.required]
    });

    this.activatedRoute.paramMap.subscribe(params => {
      this.paramsID = params.get('id');
      if (this.paramsID != null) {
        this.check = false;
        this.brandService.getByID(this.paramsID).subscribe(rs => {
          this.registerForm.setValue(rs[0])
        })
      }
      else{
        this.check = true;
      }
    })

    console.log(this.registerForm.value.ButtonName);
  }

  get f() { return this.registerForm.controls; }

  saveBrand() {
    if (this.paramsID == null) {
      this.brandService.create(this.registerForm.value).subscribe(rs => {
        this.showToast('success', 'Title', 'Tạo thương hiệu thành công..!');
      });
    }
    else {
      this.brandService.update(this.registerForm.value).subscribe(rs => {
        this.showToast('success', 'Title', 'Cập nhật thương hiệu thành công..!');
      })
    console.log("datkkkkk");

    }
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? `. ${title}` : '';
    this.toastrService.show(
      body,
      `Create`,
      config);
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.saveBrand();
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}
