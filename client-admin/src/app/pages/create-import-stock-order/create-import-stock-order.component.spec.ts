import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateImportStockOrderComponent } from './create-import-stock-order.component';

describe('CreateImportStockOrderComponent', () => {
  let component: CreateImportStockOrderComponent;
  let fixture: ComponentFixture<CreateImportStockOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateImportStockOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateImportStockOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
