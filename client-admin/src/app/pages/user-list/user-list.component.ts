import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { UserService } from '../../services/customer.service';

@Component({
  selector: 'ngx-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  rows = [];

  columns = [
    { name: 'Tên đăng nhập', dataKey:'Username'},
    { name: 'Họ và tên', dataKey:'FullName' },
    { name: 'Địa chỉ', dataKey:'Address' },
    { name: 'Số điện thoại' , dataKey:'Phonenumber'},
    { name: 'Email', dataKey:'Email' }
  ]


  constructor(private service: UserService) {

  }

  ngOnInit() {

    this.service.getUsers().subscribe(rs => {
      this.rows = rs;
    })
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.deleteUser(event.data.id).subscribe(rs => {
        event.confirm.resolve();
      });
    } else {
      event.confirm.reject();
    }
  }

  onDeleteUser(row){
    console.log('DELETE', row.Username);
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.deleteUser(row.Username).subscribe(rs => {
        row.confirm.resolve();
      });
    } else {
      row.confirm.reject();
    }
    
  }
}
