import { NgModule } from '@angular/core';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { TablesRoutingModule } from '../tables/tables-routing.module';
import { ListBrandsComponent } from './list-brands.component';
//import { MyTableComponent } from '../../components/my-table/my-table.component';
import { SharedModule } from '../../share/share.module';

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    TablesRoutingModule,
    Ng2SmartTableModule,
    SharedModule
  ],
  declarations: [
    ListBrandsComponent,  
    //MyTableComponent
  ],
})
export class ListBrandsModule { }