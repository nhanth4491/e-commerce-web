import { Component, OnInit } from '@angular/core';
import { BrandService } from '../../services/brand.service';

@Component({
  selector: 'ngx-list-brands',
  templateUrl: './list-brands.component.html',
  styleUrls: ['./list-brands.component.scss']
})
export class ListBrandsComponent implements OnInit {

  rows = [];

  columns = [
    { name: 'Tên thương hiệu', dataKey:'Name'},
    { name: 'Số điện thoại', dataKey:'Phonenumber' },
    { name: 'Địa chỉ', dataKey:'Address' }
  ]
  constructor(private brandService: BrandService) { }

  ngOnInit(): void {
    this.brandService.getBrands().subscribe(rs => {
      this.rows = rs;
    })
  }

  onDeleteBrand(value) : void{
    if (window.confirm('Are you sure you want to delete?')) {
      this.brandService.delete(value.ID).subscribe(rs => {
        for(let i = 0; i < this.rows.length; i++){
          if(this.rows[i].ID == value.ID){
            this.rows.splice(i, 1);
            break;
          }
        }
      });
    }
  }
}
