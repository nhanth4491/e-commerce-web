import { Component, OnInit } from '@angular/core';
import { NbComponentStatus, NbComponentShape, NbComponentSize } from '@nebular/theme';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'create-news',
  templateUrl: './create-news.component.html',
  styleUrls: ['./create-news.component.scss']
})
export class CreateNewsComponent implements OnInit {

  newsForm: FormGroup;
  constructor() { }
  statuses: NbComponentStatus[] = [ 'primary', 'success', 'info', 'warning', 'danger' ];
  shapes: NbComponentShape[] = [ 'rectangle', 'semi-round', 'round' ];
  sizes: NbComponentSize[] = [ 'tiny', 'small', 'medium', 'large', 'giant' ];
  ngOnInit(): void {
    
  }

}
