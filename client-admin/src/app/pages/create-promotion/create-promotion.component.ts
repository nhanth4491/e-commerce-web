import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbToastrService, NbComponentStatus, NbGlobalPhysicalPosition } from '@nebular/theme';
import { PromotionService } from '../../services/promotion.service';

import * as moment from 'moment';

@Component({
  selector: 'create-promotion',
  templateUrl: './create-promotion.component.html',
  styleUrls: ['./create-promotion.component.scss']
})
export class CreatePromotionComponent implements OnInit {
  promotionForm: FormGroup;
  submitted = false;
  id: string;


  constructor(private promotionService: PromotionService,
    private activatedRoute: ActivatedRoute,
    private toastrService: NbToastrService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.promotionForm = this.formBuilder.group({
      Name: ['', Validators.required],
      Discount: ['', Validators.required],
      Datefrom: ['', Validators.required],
      Dateto: ['', Validators.required]
    });

    this.activatedRoute.paramMap.subscribe(params => {
      this.id = params.get('id');
    })
  }
  get f() { return this.promotionForm.controls; }
  save() {
    let datefrom = moment(this.promotionForm.value.Datefrom);
    this.promotionForm.value.Datefrom = datefrom.format('YYYY-MM-DD');
    let dateto = moment(this.promotionForm.value.Dateto);
    this.promotionForm.value.Dateto = dateto.format('YYYY-MM-DD');
    console.log(this.promotionForm.value);
    this.promotionService.createPromotion(this.promotionForm.value).subscribe(rs => {
      this.showToast('success', 'Title', 'Update success..!');
    });
  }
  upadte(){

  }
  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? `. ${title}` : '';
    this.toastrService.show(
      body,
      `Toast`,
      config);
  }
  onSubmit() {
    this.submitted = true;
    if (this.promotionForm.invalid) {
      return;
    }
    if(this.id){
      this.upadte();
    }
    else{
      this.save();
    }
    
  }
  onReset() {
    this.submitted = false;
    this.promotionForm.reset();
  }
}
