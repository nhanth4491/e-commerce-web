import { Component, OnInit } from '@angular/core';
import { RoleService } from '../../services/role.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbToastrService, NbComponentStatus, NbGlobalPhysicalPosition } from '@nebular/theme';

@Component({
  selector: 'create-role',
  templateUrl: './create-role.component.html',
  styleUrls: ['./create-role.component.scss']
})
export class CreateRoleComponent implements OnInit {
  roleForm: FormGroup;
  submitted = false;
  id: string;
  constructor(private roleService: RoleService,
    private activatedRoute: ActivatedRoute,
    private toastrService: NbToastrService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void { 
    this.roleForm = this.formBuilder.group({
      Role: ['', Validators.required]
    });
    this.activatedRoute.paramMap.subscribe(params => {
      this.id = params.get('id');
    })
  }
  get f() { return this.roleForm.controls; }

  save() {
    this.roleService.createRole(this.roleForm.value).subscribe(rs => {
      this.showToast('success', 'Title', 'Success..!');
    });
  }
  update(){
    
  }
  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? `. ${title}` : '';
    this.toastrService.show(
      body,
      `Toast`,
      config);
  }
  onSubmit() {
    this.submitted = true;
    if (this.roleForm.invalid) {
      return;
    }
    this.save();
  }

}
