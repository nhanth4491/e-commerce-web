import { NgModule } from '@angular/core';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule } from '@nebular/theme';


import { ThemeModule } from '../../@theme/theme.module';
import { TestEditorComponent } from './test-editor.component';
import { CKEditorModule } from 'ng2-ckeditor';


@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    CKEditorModule
  ],
  declarations: [
    TestEditorComponent
  ],
})
export class TestEditorModule { }