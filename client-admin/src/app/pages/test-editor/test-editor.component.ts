import { Component, OnInit } from '@angular/core';
import './ckeditor.loader';
import 'ckeditor';

@Component({
  selector: 'ngx-test-editor',
  templateUrl: './test-editor.component.html',
  styleUrls: ['./test-editor.component.scss']
})
export class TestEditorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
