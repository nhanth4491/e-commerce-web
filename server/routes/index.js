var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/home', function(req, res, next) {
  res.render('home');
});
// router.get('/api/getlistproduct', function(req, res, next) {
//   res.send('[{ name: "abc", image: "/dg/sdf.jpg" }]');
// });
// router.get('/api/getproductdetail', function(req, res, next) {
//   console.log(req.query);
//   res.send('[{ name: "abc", image: "/dg/sdf.jpg" }]');
// });
//router.post()
module.exports = router;
