var mysql = require('mysql');

var mySQLProvider = {
    connection: mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "123456",
        database: "test"
    }),
    connect: function(){
        this.connection.connect(function(err) {
            if (err) throw err;
            console.log("Connected!!!")
        });
    },
    query: function(sql, callback){
        //var sql = 'select * from users';
        this.connection.query(sql, function(err, results) {
            if (err) throw err;
            callback(results);
        })
    }
}

module.exports = mySQLProvider;